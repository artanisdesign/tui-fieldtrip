function receivePush(e) {
    Alloy.Globals.pushReceived(e);
}

function deviceTokenSuccess(e) {
    deviceToken = e.deviceToken;
    Cloud.PushNotifications.subscribeToken({
        device_token: deviceToken,
        channel: "test-",
        type: "ios"
    }, function(e) {
        e.success ? console.log("Subscribed") : console.log("Error:\n" + (e.error && e.message || JSON.stringify(e)));
    });
}

function deviceTokenError(e) {
    console.log("Failed to register for push notifications! " + e.error);
}

var Alloy = require("alloy"), _ = Alloy._, Backbone = Alloy.Backbone;

var Cloud = require("ti.cloud");

var deviceToken = null;

Ti.Network.registerForPushNotifications({
    types: [ Ti.Network.NOTIFICATION_TYPE_BADGE, Ti.Network.NOTIFICATION_TYPE_ALERT, Ti.Network.NOTIFICATION_TYPE_SOUND ],
    success: deviceTokenSuccess,
    error: deviceTokenError,
    callback: receivePush
});

Ti.UI.backgroundColor = "red";

Alloy.createController("index");