function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.bg = Ti.UI.createWindow({
        backgroundImage: "images/home_default.jpg",
        id: "bg",
        backgroundColor: "red"
    });
    $.__views.bg && $.addTopLevelView($.__views.bg);
    $.__views.home = Ti.UI.createWindow({
        navBarHidden: true,
        width: "100%",
        translucent: false,
        barColor: "#a1c8e7",
        hideShadow: true,
        backgroundImage: "images/home_default.jpg",
        title: "TUI FieldTrip",
        id: "home",
        zIndex: "2"
    });
    $.__views.__alloyId1 = Ti.UI.createScrollView({
        height: Ti.UI.FILL,
        contentHeight: Ti.UI.FILL,
        contentWidth: "320",
        scrollType: "vertical",
        id: "__alloyId1"
    });
    $.__views.home.add($.__views.__alloyId1);
    $.__views.webview = Ti.UI.createWebView({
        backgroundColor: "transparent",
        top: 0,
        height: "100%",
        width: 320,
        id: "webview",
        scalesPageToFit: "false"
    });
    $.__views.__alloyId1.add($.__views.webview);
    $.__views.nav = Ti.UI.iOS.createNavigationWindow({
        window: $.__views.home,
        id: "nav",
        width: "100%",
        zIndex: "2"
    });
    $.__views.bg.add($.__views.nav);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.webview.url = "http://hereandnow.eu01.aws.af.cm/app/index.html#";
    var pushData;
    Alloy.Globals.pushReceived = function(e) {
        pushData = e.data;
        tf.text = pushData.alert;
        pushView.animate({
            left: 0,
            top: 0,
            duration: 150,
            curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
        });
    };
    var pushView = Ti.UI.createView({
        height: 92,
        backgroundColor: "#55a6aa",
        zIndex: 5,
        top: -92,
        width: 320
    });
    var repimage = Ti.UI.createView({
        height: 66,
        width: 69,
        backgroundImage: "images/rep.png",
        left: 0,
        top: 18
    });
    pushView.add(repimage);
    var cont = Ti.UI.createView({
        right: 15,
        left: 70,
        top: 20,
        bottom: 10
    });
    var tf = Ti.UI.createLabel({
        height: Ti.UI.SIZE,
        font: {
            fontFamily: "TUITypeLight",
            fontSize: 16
        },
        color: "white",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        text: "",
        left: 0,
        right: 0
    });
    cont.add(tf);
    pushView.add(cont);
    $.nav.add(pushView);
    pushView.addEventListener("swipe", function() {
        pushView.animate({
            left: 0,
            top: -92,
            duration: 150,
            curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
        });
    });
    pushView.addEventListener("click", function() {
        $.webview.url = "http://hereandnow.eu01.aws.af.cm/app/index.html#/excursion/" + pushData.id;
        pushView.animate({
            left: 0,
            top: -92,
            duration: 150,
            curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
        });
    });
    $.bg.open();
    $.nav.open();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;