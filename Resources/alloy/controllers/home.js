function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "home";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.homeWindow = Ti.UI.createWindow({
        navBarHidden: false,
        width: "100%",
        translucent: "false",
        barColor: "#a1c8e7",
        hideShadow: true,
        backgroundImage: "images/home_default.jpg",
        title: "TUI FieldTrip",
        id: "homeWindow",
        backButtonTitle: "back"
    });
    $.__views.homeWindow && $.addTopLevelView($.__views.homeWindow);
    $.__views.__alloyId0 = Ti.UI.createScrollView({
        height: Ti.UI.FILL,
        contentHeight: Ti.UI.FILL,
        contentWidth: "320",
        scrollType: "vertical",
        id: "__alloyId0"
    });
    $.__views.homeWindow.add($.__views.__alloyId0);
    $.__views.webview = Ti.UI.createWebView({
        backgroundColor: "transparent",
        top: 0,
        height: "100%",
        width: 320,
        id: "webview"
    });
    $.__views.__alloyId0.add($.__views.webview);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    $.webview.url = "http://192.168.0.225:8000/app/index.html#/excursion/" + args.id;
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;