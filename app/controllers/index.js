var toggle = false, moving = false, axis;
function openBlueWindow(e) {
    var home = Alloy.createController('home').getView();
    $.nav.openWindow(home, {
        animated : true
    });
}

$.webview.url = "http://hereandnow.eu01.aws.af.cm/app/index.html#";

function offOpen() {
    // If the menu is opened
    if (toggle == true) {
        $.nav.animate({
            left : 0,
            duration : 180,
            curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
        });
        toggle = false;

    }
    // If the menu isn't opened
    else {
        $.nav.animate({
            left : 150,
            duration : 180,
            curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
        });
        toggle = true;
    }
};

var pushData;
Alloy.Globals.pushReceived = function(e) {
    pushData = e.data;
    tf.text = pushData.alert;
    pushView.animate({
        left : 0,
        top : 0,
        duration : 150,
        curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
    });

};

var pushView = Ti.UI.createView({
    height : 92,
    backgroundColor : "#55a6aa",
    zIndex : 5,
    top : -92,
    width : 320
});

var repimage = Ti.UI.createView({
    height : 66,
    width : 69,
    backgroundImage : "images/rep.png",
    left : 0,
    top : 18
});
pushView.add(repimage);

var cont = Ti.UI.createView({
    right : 15,
    left : 70,
    top : 20,
    bottom : 10
});

var tf = Ti.UI.createLabel({
    height : Ti.UI.SIZE,
    font : {
        fontFamily : "TUITypeLight",
        fontSize : 16
    },
    color : "white",
    textAlign : Ti.UI.TEXT_ALIGNMENT_LEFT,
    text : "",
    left : 0,
    right : 0
});

cont.add(tf);
pushView.add(cont);
$.nav.add(pushView);

pushView.addEventListener("swipe", function() {
    pushView.animate({
        left : 0,
        top : -92,
        duration : 150,
        curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
    });
});

pushView.addEventListener("click", function() {
    /*  var win = Alloy.createController("home", {
     id : pushData.id
     }).getView();
     $.nav.openWindow(win);*/
    $.webview.url = "http://hereandnow.eu01.aws.af.cm/app/index.html#/excursion/" + pushData.id;
    pushView.animate({
        left : 0,
        top : -92,
        duration : 150,
        curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
    });
});
/*
 $.home.addEventListener("swipe", function(e) {

 if (e.direction === "left") {
 $.nav.animate({
 left : 0,
 duration : 180,
 curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
 });
 toggle = false;
 }
 if (e.direction === "right") {
 $.nav.animate({
 left : 150,
 duration : 180,
 curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
 });
 toggle = true;
 }

 });
 ÷/
 /*
 function touchstart(e) {
 // Get starting horizontal position
 axis = parseInt(e.x);

 }

 function touchmove(e) {
 // Subtracting current position to starting horizontal position

 var coordinates = parseInt(e.x) - axis;
 console.log(coordinates);
 // Detecting movement after a 20px shift
 if (coordinates > 20 || coordinates < -20) {
 moving = true;
 }
 // Locks the window so it doesn't move further than allowed
 if (moving == true && coordinates <= 150 && coordinates >= 0) {
 // This will smooth the animation and make it less jumpy
 $.home.animate({
 left : coordinates,
 duration : 20
 });
 // Defining coordinates as the final left position
 $.home.left = coordinates;

 }
 }
 */
$.bg.open();
$.nav.open();
