

// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};
// Require in the Cloud module

var Cloud = require("ti.cloud");

var deviceToken = null;

Ti.Network.registerForPushNotifications({
    // Specifies which notifications to receive
    types : [Ti.Network.NOTIFICATION_TYPE_BADGE, Ti.Network.NOTIFICATION_TYPE_ALERT, Ti.Network.NOTIFICATION_TYPE_SOUND],
    success : deviceTokenSuccess,
    error : deviceTokenError,
    callback : receivePush
});
// Process incoming push notifications
function receivePush(e) {
   Alloy.Globals.pushReceived(e);
}

// Save the device token for subsequent API calls
function deviceTokenSuccess(e) {
    deviceToken = e.deviceToken;

    // Subscribes the device to the 'test' channel
    // Specify the push type as either 'android' for Android or 'ios' for iOS
    Cloud.PushNotifications.subscribeToken({
        device_token : deviceToken,
        channel : 'test-',
        type : Ti.Platform.name == 'android' ? 'android' : 'ios'
    }, function(e) {
        if (e.success) {
            console.log('Subscribed');
        } else {
            console.log('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
        }
    });

}

function deviceTokenError(e) {
    console.log('Failed to register for push notifications! ' + e.error);
}
Ti.UI.backgroundColor = "red";

